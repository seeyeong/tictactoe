import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square({value, onClick}) {
    return <button className="square" onClick={onClick}>{value}</button>;
}

function renderSquare(value, i, onClick) {
    return <Square value={value} onClick={() => onClick(i)} />;
}

function Board({squares, onClick}) {
    return (
        <div>
            {[0, 3, 6].map((i) => (
                <div key={i.toString()} className="board-row">
                    {renderSquare(squares[0 + i], (0 + i), onClick)}
                    {renderSquare(squares[1 + i], (1 + i), onClick)}
                    {renderSquare(squares[2 + i], (2 + i), onClick)}
                </div>
            ))}
        </div>
    );
}

function Game() {
	const [allHistory, setAllHistory] = useState([])
	const [history, setHistory] = useState([{squares: Array(9).fill(null)}])
	const [xIsNext, setXIsNext] = useState(true)	
	const [stepNumber, setStepNumber] = useState(0)
    const player = xIsNext ? "X" : "O"
	const current = history[stepNumber];
	const squares = current.squares.slice();
	const winner = calculateWinner(squares);
	let status;
	if(winner) {
		status = `Winner: ${winner}`;
	}else{
		status = `Next player: ${player}`;
	}

	const jumpTo = (step) => {
		setStepNumber(step)
		setXIsNext(xIsNext % 2 === 0);
	}
 
	const moves = history.map((step, move) => {
		console.log("step", step)
		const desc = move ? `Go to move ${move}` : 'Go to game start';
		return (
			<li key={move}>
				<button onClick={() => jumpTo(move)}>{desc}</button>
			</li>
		)
	});

	const handleNewGame = () => {
		//push history to new Array
		let game = {
			playerX: "sy",
			playerO: "ys",
			winner,
			table: 1,
			history,
			xIsNext,
			stepNumber
		}

		//only save when got winner or game ended.
		if(stepNumber === 9 || winner) {
			setAllHistory(allHistory.concat(game))
			//reset history and setStepNumber to 0
			setHistory([{squares: Array(9).fill(null)}])
			setStepNumber(0)
			setXIsNext(true)
		}
	}

	const handleRecall = (index) => {
		//game index
		console.log("game index", index);
		console.log("all history", allHistory[index]);

		//replace current game state.
		let {history, xIsNext, stepNumber} = allHistory[index];
		setHistory(history)
		setXIsNext(xIsNext)
		setStepNumber(stepNumber)
	}

    return (
        <div className="game">
            <div className="game-board">
                <Board squares={current.squares} onClick={(i) => {
					let current = history.slice(0, stepNumber+1)[history.length - 1]
					let squares = current.squares.slice()
					if (calculateWinner(squares) || squares[i]) {
						return;
					}
					squares[i] = xIsNext ? 'X' : 'O';
					setHistory(history.concat([{
						squares: squares,
					  }]))
					setStepNumber(history.length)
					setXIsNext(!xIsNext);
				}}/>
            </div>
            <div className="game-info">
                <div>{status}</div>
                <ol>{moves}</ol>
            </div>

			<div>
				<button onClick={() => handleNewGame()}>Save & Start new game</button>
				<ol>
					{
						allHistory.map((game, index) => <li key={index.toString()}>Game {index+1} history, Winner: {game.winner} <button onClick={() => handleRecall(index)}>Recall</button></li>)
					}
				</ol>
			</div>
        </div>
    );
}

function calculateWinner(squares) {
	const lines = [
		[0,1,2],
		[3,4,5],
		[6,7,8],
		[0,3,6],
		[1,4,7],
		[2,5,8],
		[0,4,8],
		[2,4,6],
	];
	for (let i = 0; i < lines.length; i++) {
		const [a,b,c] = lines[i];
		if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
			return squares[a];
		}
	}
	return null;
}

ReactDOM.render(<Game />, document.getElementById('root'));
